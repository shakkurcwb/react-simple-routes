import React from 'react';
import { BrowserRouter, Route, Link } from 'react-router-dom';
import routes from './config.js';

function Menu(props) {
  return (
    <div className="menu">
      <ul>
      {
        routes.map((route, index) => (
          <li key={index}>
            <Link to={route.path} title={route.title}>
              <i className={"fas fa-" + route.icon}></i> {route.title}
            </Link>
          </li>
        ))
      }
      <li key="user" className="sub-menu">
        <i className={"far fa-bell"}></i>
      </li>
      </ul>
    </div>
  );
}

function Content(props) {
  return (
    <div className="container">
    {
      routes.map((route, index) => (
        <Route
          key={index}
          path={route.path}
          exact={route.exact}
          component={route.component}
        />
      ))
    }
    </div>
  );
}

function Footer(props) {
  return (
    <footer className="center-align">
      Copyrights @ 2018
    </footer>
  );
}

function Routes(props) {
  return (
    <BrowserRouter>
      <div className="view">
        <Menu />
        <Content />
        <Footer />
      </div>
    </BrowserRouter>
  );
}

export default Routes;