import Home from '../views/home.js';
import Person from '../views/person/person.js';

import Fake from '../views/fake.js';

const routes = [
  {
    path: "/",
    exact: true,
    component: Home,
    title: 'Home',
    icon: 'home',
  },
  {
    path: "/person",
    exact: true,
    component: Person,
    title: 'People',
    icon: 'user',
  },
  {
    path: "/animal",
    exact: true,
    component: Fake,
    title: 'Animals',
    icon: 'dove',
  },
];

export default routes;