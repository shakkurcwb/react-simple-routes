import React from 'react';

const json = [
  { id: 1, name: 'Marlon A J Ferreira', age: 25 },
  { id: 2, name: 'Mayron T Ceccon', age: 27 },
  { id: 3, name: 'Giuliano A Cella', age: 37 },
];

class Person extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="person-panel">
        <h3>Person Panel</h3>
        <Form />
        <List d={json}/>
      </div>
    );
  }
}

export default Person;

function List(props) {
  return (
    <table className="highlight responsible-table">
      <thead>
        <tr>
          <th>Id</th>
          <th>Name</th>
          <th>Age</th>
        </tr>
      </thead>
      <tbody>
      {
        props.d.map((object, index) => {
          return ([
            <tr key={object.id}>
              <td>{object.id}</td>
              <td>{object.name}</td>
              <td>{object.age}</td>
            </tr>
          ]);
        })
      }
      </tbody>
    </table>
  );
}

function Form(props) {
  return (
    <div className="form">
      <SearchForm />
    </div>
  );
}

function SearchForm(props) {
  return (
    <div className="search-form">
      <input type="text" value="" placeholder="Entry a name to search" />
    </div>
  );
}

