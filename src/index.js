import React from 'react';
import { render } from 'react-dom';

import Routes from './routes/routes.js';
import './style.css';

render(
  <Routes />, document.getElementById('root')
);
